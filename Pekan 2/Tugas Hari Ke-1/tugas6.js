//soal 1
var daftarPesertaArr = ["Asep", "laki-laki", "baca buku" , 1992]
var daftarPesertaObj = {
  nama : "Asep",
  jenisKelamin : "laki-laki",
  hobi : "baca buku",
  tahunLahir : 1992
}

//soal 2
var buah = [
    {nama: "strawberry", warna: "merah", adaBijinya: "tidak", harga: 9000}, 
    {nama: "jeruk", warna: "oranye", adaBijinya: "ada", harga: 8000}, 
    {nama: "semangka", warna: "hijau & merah", adaBijinya: "ada", harga: 10000}, 
    {nama: "pisang", warna: "kuning", adaBijinya: "tidak", harga: 5000}]
  
  console.log(buah[0])

//soal 3
var dataFilm = []
var dataFilmObj = {
    nama: "John Wick",
    durasi: "1.41 jam",
    genre: "Action",
    tahun: 2014
}
dataFilm.push(dataFilmObj)

console.log(dataFilm)

//soal 4 - release 0
class Animal {
  constructor(name,legs,cold_blooded) {
      this.name = name;
      this.legs = 4;
      this.cold_blooded = false;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

//soal 4 - release 1